<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Registro de categorías</title>
</head>

<body>

  <div>
    <a href="agregar.php">Nueva categoría</a>
    <table>
      <thead>
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th>Opciones</th>



        </tr>

      </thead>

      <tbody>
        <?php
        include 'conexion.php';
        $sql = "SELECT * from datos_categorias";
        $resultado = mysqli_query($enlace,$sql);
        while($filas=mysqli_fetch_array($resultado)){

        ?>
      
        <tr>
          <td><?php echo $filas['id'] ?></td>
          <td><?php echo $filas['nombre'] ?></td>
          <td><?php echo $filas['descripcion'] ?></td>
          <td>
            <a href="eliminar.php?id=<?php echo $filas['id'] ?>" >Eliminar</a>
          </td>

        </tr>

      </tbody>
      <?php
        }
      ?>
    </table>

  </div>

</body>

</html>